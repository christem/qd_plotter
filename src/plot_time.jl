using Statistics: mean
# using PyPlot
using PlotlyJS
using qd_analysis

include("helpers.jl")



function plot_time_domain(tds::Vector{Capture{T}} where T; 
                          legend_entries::Vector{String}=generate_default_legend(length(tds)), 
                          events::Dict{String, Float64}=Dict{String,Float64}(),
                          x_limits::Union{Nothing, Tuple{Real, Real}} = nothing,
                          y_limits::Union{Nothing, Tuple{Real, Real}} = nothing,
                          plot_title::String = "Time Domain Signal",
                          time_scale::Symbol = :rel,
                          type::Symbol = :none,
                          shaded_region::Union{Tuple{Float64, Float64}, Nothing} = nothing,
                          alt_x_axis::Union{Vector{<:Vector{<:Number}}, Vector{<:Number}, Nothing} = nothing,
                          alt_x_axis_title::Union{Nothing, String} = nothing,
                          y_axis_title::Union{Nothing, String, Symbol} = nothing,
                          height::Union{Nothing, Int} = nothing,
                          width::Union{Nothing, Int} = nothing,
                          channel_rel_time_ref::Union{Nothing, Integer} = nothing ) #  Only relevant when time_scale is :channel_rel
    
    tds = if type == :none
        tds
    elseif type == :abs
        [Capture(abs.(cap.val), sample_rate=cap.sample_rate, time_start=cap.time_start, stimuli=cap.stimuli) for cap in tds]
    elseif type == :phase
        [Capture(angle.(cap.val), sample_rate=cap.sample_rate, time_start=cap.time_start, stimuli=cap.stimuli) for cap in tds]
    elseif type == :real
        [Capture(real.(cap.val), sample_rate=cap.sample_rate, time_start=cap.time_start, stimuli=cap.stimuli) for cap in tds]
    elseif type == :imag
        [Capture(imag.(cap.val), sample_rate=cap.sample_rate, time_start=cap.time_start, stimuli=cap.stimuli) for cap in tds]
    else
        throw(ArgumentError("Invalid type!"))
    end
    traces = make_traces(tds, legend_entries=legend_entries, alt_x_axis=alt_x_axis, events=events, time_scale=time_scale, channel_rel_time_ref=channel_rel_time_ref)

    if isnothing(y_axis_title)
        ytitle = "Normalized Amplitude"
    elseif y_axis_title isa String
        ytitle = y_axis_title
    elseif y_axis_title isa Symbol
        ytitle = get_yaxis_title(y_axis_title, type)
    else
        throw(ArgumentError("Invalid y_axis_title!"))
    end

    if isnothing(alt_x_axis_title)
        xaxis_title = get_timescale_string(time_scale)
    else
        xaxis_title = alt_x_axis_title
    end
    layout = Layout(;title=plot_title, xaxis_title=xaxis_title, yaxis_title=ytitle, showlegend=true, xaxis_range = x_limits, yaxis_range = y_limits, height=height, width=width)

    if !isnothing(shaded_region)
        x0, x1 = shaded_region
        min_y = minimum([minimum(capture) for capture in tds])
        max_y = maximum([maximum(capture) for capture in tds])
        diff = max_y - min_y

        shaded_shape = attr(
            type="rect",
            xref="x",
            yref="y",
            x0=x0,
            y0=min_y - 0.1*diff,  # Slightly below the min value
            x1=x1,
            y1=max_y + 0.1*diff,  # Slightly above the max value
            fillcolor="gray",
            opacity=1.0,
            line_width=0,
            layer="below"
        )
        layout[:shapes] = [shaded_shape]
    end

    return PlotlyJS.Plot(traces, layout)
end


function plot_time_domain(
    tds::Capture{T} where T; # Single object
    kwargs... # Capture all keyword arguments
)
    # Wrap the single FrequencyResponse object in a vector
    tds_vector = [tds]
    
    # Forward all arguments to the base method
    return plot_time_domain(tds_vector; kwargs...)
end






