using PlotlyJS
using qd_analysis

include("helpers.jl")

using PlotlyJS


# Function to generate a trace for PlotlyJS
function generate_trace(freq_resp::FrequencyResponse, legend_entry::AbstractString, color::String, mode::String, show_legend::Bool, response_type::Symbol)
    freqs = freq_resp.frequency
    resp = select_response_type(freq_resp, response_type)

    return PlotlyJS.scatter(
        x = freqs,
        y = resp,
        mode = mode,
        line = Dict(:color => color, :width => 2),
        name = legend_entry,
        showlegend = show_legend,
    )
end

# Main plotting function
function plot_frequency_responses(
    freq_resps::Vector{FrequencyResponse};
    legend_entries::Vector{<:AbstractString} = generate_default_legend(length(freq_resps)),
    plot_title::String = "Frequency Domain",
    x_limits::Union{Nothing, Tuple{Real, Real}} = nothing,
    y_limits::Union{Nothing, Tuple{Real, Real}} = nothing,
    y_axis_label::Union{Nothing,String} = nothing,
    response_type::Symbol = :mag,
    plot_as_dots::Bool = false,
    y_axis_log::Bool = true,
    x_axis_log::Bool = true,
    height::Union{Nothing, Int} = nothing,
    width::Union{Nothing, Int} = nothing,
    colour_ids::Vector{<:Integer} = collect(1:length(freq_resps)),
    legend_present::BitVector = trues(length(freq_resps)),
    highlight_bins:: Union{Nothing, Vector{Tuple{Int, Int}}} = nothing
)
    if length(freq_resps) != length(legend_entries)
        throw(ArgumentError("The number of FrequencyResponse objects must match the number of legend entries!"))
    end
    if length(freq_resps) != length(colour_ids)
        throw(ArgumentError("The number of FrequencyResponse objects must match the number of colour entries!"))
    end

    if isnothing(y_axis_label)
        y_axis_label = get_y_axis_label(response_type, "Impedance Magnitude (Ohms)")
    end
    
    log_x_limits = x_limits === nothing ? nothing : log10.(x_limits)
    mode = plot_as_dots ? "lines+markers" : "lines"
    traces = Vector{GenericTrace{Dict{Symbol, Any}}}()

    for (i, freq_resp) in enumerate(freq_resps)
        color = get_color_for_index(colour_ids[i])
        trace = generate_trace(freq_resp, legend_entries[i], color, mode, legend_present[i], response_type)
        push!(traces, trace)
    end

    # If highlight_bins is provided, add a single red dot to the plot for each bin. The dot is located at the bin's frequency and has a magnitude equal to the bin's value of the specified series. The first integer of the tuple is bin id, the second is the series id.

    if highlight_bins !== nothing
        for (bin_id, series_id) in highlight_bins
            bin_freq = freq_resps[series_id].frequency[bin_id]
            bin_val = select_response_type(freq_resps[series_id], response_type)[bin_id]
            highlight_trace = PlotlyJS.scatter(
                x = [bin_freq],
                y = [bin_val],
                mode = "markers",
                marker = Dict(:color => "red", :size => 10),
                name = "Bin $bin_id",
                showlegend = false
            )
            push!(traces, highlight_trace)
        end
    end


    layout = Layout(
        title = plot_title,
        xaxis_title = "Frequency (Hz)",
        yaxis_title = y_axis_label,
        xaxis_type = x_axis_log ? "log" : "linear",
        yaxis_type = response_type == :phase ? "linear" : (y_axis_log ? "log" : "linear"),
        xaxis_showgrid = true,
        yaxis_showgrid = true,
        yaxis_showline = false,
        xaxis_zeroline = false,
        yaxis_zeroline = false,
        xaxis_range = log_x_limits,
        height = height,
        width = width,
        yaxis_range = y_limits === nothing ? nothing : (y_axis_log ? log10.(y_limits) : y_limits)
    )

    return PlotlyJS.Plot(traces, layout)
end

function plot_frequency_responses(
    freq_resp::FrequencyResponse; # Single object
    kwargs... # Capture all keyword arguments
)
    # Wrap the single FrequencyResponse object in a vector
    freq_resps_vector = [freq_resp]
    
    # Forward all arguments to the base method
    return plot_frequency_responses(freq_resps_vector; kwargs...)
end

# Selects the response type for plotting
function select_response_type(response, response_type::Symbol)
    if response_type == :phase
        return angle.(response.response)
    elseif response_type == :real
        return real.(response.response)
    elseif response_type == :imag
        return imag.(response.response)
    elseif response_type == :ind
        return imag.(response.response) ./ (2π * response.frequency)
    else
        return abs.(response.response)
    end
end

# Determines y-axis label based on response type
function get_y_axis_label(response_type::Symbol, default_label::String)::String
    if response_type == :mag
        "Impedance Magnitude (Ohms)"
    elseif response_type == :phase
         "Impedance Phase (Radians)"
    elseif response_type == :real
        "Real Part of Impedance"
    elseif response_type == :imag
        "Imaginary Part of Impedance"
    elseif response_type == :ind
        "Inductance (H)"
    else
        default_label
    end
end
