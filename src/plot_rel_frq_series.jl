using PlotlyJS
using qd_analysis

include("helpers.jl")

using PlotlyJS

"""
    plot_rel_frq_series(stft::STFT; auxiliary_data::Union{Vector{<:Float64}, Nothing}=nothing, auxiliary_time::Union{Vector{<:Float64}, Nothing}=nothing, auxiliary_name::String="Temperature", shaded_region::Union{Tuple{Float64, Float64}, Nothing}=nothing, type::Symbol=:abs, mode=:rel, time_scale=:rel)

Plot the relative frequency series from an STFT object.

# Arguments
- `stft::STFT`: The Short-Time Fourier Transform (STFT) object containing frequency and response data.
- `auxiliary_data::Union{Vector{<:Float64}, Nothing}` (optional): A vector of auxiliary data values to be plotted, such as temperature or other time-dependent data.
- `auxiliary_time::Union{Vector{<:Float64}, Nothing}` (optional): A vector of time points corresponding to the `auxiliary_data`.
- `auxiliary_name::String`: Label for the auxiliary y-axis. Default is `"Temperature"`.
- `shaded_region::Union{Tuple{Float64, Float64}, Nothing}` (optional): A tuple specifying the start and end points of a shaded region on the x-axis. Default is `nothing`.
- `type::Symbol`: Specifies the type of data to plot. Options are `:abs` (default), `:phase`, `:real`, `:imag`, `:ind`.
- `mode::Symbol`: Specifies whether to normalize data relative to the first sample. Options are `:rel` (default) or `:abs`.
- `time_scale::Symbol`: Specifies the time scale for plotting. Options are `:rel` (relative) or other custom scales.

# Returns
- A `PlotlyJS.Plot` object representing the plot of the relative frequency series.
"""
function plot_rel_frq_series(stft::STFT; 
    time_scale = :rel,  # Move time_scale before xaxis_title
    xaxis_title::String = get_timescale_string(time_scale),
    alt_x_axis::Union{Vector{<:Float64}, Nothing} = nothing,
    alt_scaler::Union{FrequencyResponse, Nothing} = nothing,
    auxiliary_data::Union{Vector{<:Float64}, Nothing} = nothing,
    auxiliary_time::Union{Vector{<:Float64}, Nothing} = nothing,
    auxiliary_name::String = "Temperature",
    shaded_region::Union{Tuple{Float64, Float64}, Nothing} = nothing,
    type::Symbol = :abs, 
    y_axis_title::Union{Nothing, String, Symbol} = nothing,
    mode = :rel,
    height::Union{Nothing, Int} = nothing,
    width::Union{Nothing, Int} = nothing,
    plot_title::Union{Nothing, String} = nothing)
    
    # Extract relevant data from STFT object
    num_frq = length(stft.frequency)
    sample_rate = stft.sample_rate
    time_start = stft.time_start

    captures = [Capture(stft.response[idx, :], sample_rate=sample_rate, time_start=time_start) for idx in 1:num_frq]
        # Normalize captures if mode is relative
    if mode == :rel
        if isnothing(alt_scaler)
            captures = [trace / trace[1] for trace in captures]
        else
            #check that alt_scaler has same frequencies as stft
            if length(alt_scaler.frequency) != num_frq
                throw(ArgumentError("alt_scaler must have the same number of frequencies as the STFT object"))
            end
            captures = [trace / alt_scaler[idx][2] for (idx, trace) in enumerate(captures)]
        end
    end
    # Create captures based on the specified type
    captures = if type == :abs
        [Capture(abs.(cap.val), sample_rate=sample_rate, time_start=time_start) for cap in captures]
    elseif type == :phase
        [Capture(angle.(cap.val), sample_rate=sample_rate, time_start=time_start) for cap in captures]
    elseif type == :real
        [Capture(real.(cap.val), sample_rate=sample_rate, time_start=time_start) for cap in captures]
    elseif type == :imag
        [Capture(imag.(cap.val), sample_rate=sample_rate, time_start=time_start) for cap in captures]
    elseif type == :ind
        [Capture(imag.(cap.val) / (2π * stft.frequency[idx]), sample_rate=sample_rate, time_start=time_start) for (idx,cap) in enumerate(captures)]
    else
        throw(ArgumentError("Invalid type!"))
    end

    # Create legend entries from the frequency values
    legend_entries = string.(round.(stft.frequency))

    # Generate traces for plotting
    traces = make_traces(captures, alt_x_axis=alt_x_axis,legend_entries=legend_entries, time_scale=time_scale)

    # Plot auxiliary data if provided
    if !isnothing(auxiliary_data) && !isnothing(auxiliary_time)
        if time_scale == :rel
            auxiliary_time = auxiliary_time .- auxiliary_time[1]
        end
        aux_trace = scatter(x=auxiliary_time, y=auxiliary_data, mode="lines", line=attr(color="red"), name=auxiliary_name, yaxis="y2")
        push!(traces, aux_trace)
    end

    # Create layout with or without auxiliary y-axis
    if isnothing(plot_title)
        if mode == :rel
            p_title = "Relative Impedance Over Time"
        else
            p_title = "Impedance Over Time"
        end
    else 
        p_title = plot_title
    end

    if isnothing(y_axis_title)
        if mode == :rel
            ytitle = get_yaxis_title(:generic_rel, type)
        elseif mode == :abs
            ytitle = get_yaxis_title(:impedance_abs, type)
        end
    elseif y_axis_title isa String
        ytitle = y_axis_title
    elseif y_axis_title isa Symbol
        ytitle = get_yaxis_title(y_axis_title, type)
    else
        throw(ArgumentError("Invalid y_axis_title!"))
    end


    layout = Layout(; 
    title=p_title, 
    xaxis_title=xaxis_title, 
    yaxis_title=ytitle, 
    showlegend=true,
    xaxis=attr(spikemode="across"),  # Set spikemode for x-axis
    height=height,
    width=width
    )

    # Add the auxiliary y-axis if auxiliary data is provided
    if !isnothing(auxiliary_data) && !isnothing(auxiliary_time)
        layout[:yaxis2] = attr(title=auxiliary_name, side="right", overlaying="y")
    end

    # Add shaded region if specified
    if !isnothing(shaded_region)
        x0, x1 = shaded_region
        min_y = minimum([minimum(capture) for capture in captures])
        max_y = maximum([maximum(capture) for capture in captures])
        diff = max_y - min_y

        shaded_shape = attr(
            type="rect",
            xref="x",
            yref="y",
            x0=x0,
            y0=min_y - 0.1*diff,  # Slightly below the min value
            x1=x1,
            y1=max_y + 0.1*diff,  # Slightly above the max value
            fillcolor="gray",
            opacity=1.0,
            line_width=0,
            layer="below"
        )
        layout[:shapes] = [shaded_shape]
    end

    return PlotlyJS.Plot(traces, layout)
end
