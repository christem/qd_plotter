using PlotlyJS
using qd_analysis

include("helpers.jl")

using PlotlyJS

function plot_spectrogram(stft::STFT; time_scale::Symbol=:rel, freq_scale::Symbol=:linear, z_scale::Symbol=:log)
    # Extract relevant data from STFT object
    freqs = stft.frequency
    times = collect(1:size(stft.response, 2))*stft.sample_rate
    if time_scale == :abs
        times = times
        xaxis_title = "Absolute Time (s)"
    elseif time_scale == :rel
        times = (times .- stft.time_start)
        xaxis_title = "Relative Time (s)"
    end

    if freq_scale == :linear
        yaxis_type = "linear"
        yaxis_title = "Frequency (Hz)"
    elseif freq_scale == :log
        yaxis_type = "log"
        yaxis_title = "Frequency (Hz) - Log scale"
    end

    if z_scale == :log
        resp = log10.(abs.(stft.response))  # Taking the magnitude of the complex response
    else
        resp = abs.(stft.response)
    end
    
    # Create a heatmap trace]
    trace = heatmap(
        x=times,
        y=freqs,
        z=resp,
        colorscale="Viridis",
        type="heatmap"
    )
    
    # Define layout
    layout = Layout(
        title="Spectrogram",
        xaxis_title=xaxis_title,
        yaxis_title=yaxis_title,
        yaxis_type=yaxis_type,
        #yaxis_dtick=10000,  # depending on your frequency range, adjust for reasonable ticks
        xaxis_showgrid=false,
        yaxis_showgrid=false,
        yaxis_showline=false,
        xaxis_zeroline=false,
        yaxis_zeroline=false
    )
    
    # Create and display the plot
    PlotlyJS.Plot(trace, layout)
end