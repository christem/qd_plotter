module qd_plotter

# export lttb
# include("lttb.jl")

export plot_time_domain
include("plot_time.jl")

export plot_frequency_responses
include("plot_frequency_response.jl")

export plot_frequency_series
include("plot_frequency_series.jl")

export plot_spectrogram
include("plot_spectrogram.jl")

export plot_rel_frq_series
include("plot_rel_frq_series.jl")

end
