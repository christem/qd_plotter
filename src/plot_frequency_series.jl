
using PlotlyJS
using qd_analysis

include("helpers.jl")

using PlotlyJS

#TODO rel, abs, samples for time_scale. Also add channel to use as base for relative time
#Plot time series on top of num_frequencies subplots
#Add default time_scale for plot_time_domain

function plot_frequency_series(stft::STFT, kwargs...)
    plot_frequency_series([stft], kwargs...)
end


function plot_frequency_series(stfts::Vector{STFT};
    time_scale::Symbol=:rel ,
    channel_rel_time_ref::Union{Nothing, STFT} = nothing, # Only relevant when time_scale is :channel_rel
    events::Dict{String, Float64}=Dict{String,Float64}(),
    title::String = "Frequency Series",
    legend_entries::Vector{<:AbstractString} = generate_default_legend(length(stfts)),
    plot_as_dots::Bool = false,
    phase::Bool = false,
    optional_capture::Union{Vector{Capture{T}}, Nothing} where T = nothing
    )

    # Check if they have the same frequency components
    frequency_components = [stft.frequency for stft in stfts]
    if !all(x -> x == frequency_components[1], frequency_components)
        throw(ArgumentError("Frequency components are not identical!"))
    end

    freqs = stfts[1].frequency
    mode = plot_as_dots ? "lines+markers" : "lines"

    num_frequencies = length(freqs)


    if !isnothing(optional_capture)
        p = make_subplots(rows=num_frequencies + 1, cols=1, shared_xaxes=true, vertical_spacing=0.02)
    else
        p = make_subplots(rows=num_frequencies, cols=1, shared_xaxes=true, vertical_spacing=0.02)
    end
    

    xaxis_title = get_timescale_string(time_scale)
    for i in 1:num_frequencies
        tmp = Vector{Capture{Float64}}(undef, length(stfts))
        for (idx, stft) in enumerate(stfts)
            cap = stft[i, :]
            resp = phase ? angle(cap) : abs(cap)
            tmp[idx] = resp
        end
        traces = make_traces(tmp, legend_entries=legend_entries, plot_as_dots=plot_as_dots, showlegend=fill(i==1, length(stfts)), showlegend_events=fill(i==1, length(events)), events=events, time_scale=time_scale, channel_rel_time_ref=channel_rel_time_ref)
        for trace in traces
            add_trace!(p, trace, row=i, col=1)
        end
    end

    for i in 1:num_frequencies
        if i == 1
            relayout!(p, Dict(:yaxis_title => string(freqs[num_frequencies], " Hz")))
        else
            relayout!(p, Dict(Symbol("yaxis$(i)_title_text") => string(freqs[num_frequencies+1-i], " Hz")))
        end
    end

    if !isnothing(optional_capture)
        traces = make_traces(optional_capture, legend_entries=["Time Capture"], events=events, showlegend_events=fill(false, length(events)), time_scale=time_scale, colour_idx_offset=length(stfts)+length(events))
        for trace in traces
            add_trace!(p, trace, row=num_frequencies+1, col=1)
        end
    end
    
    relayout!(p, title_text=title)
    return p
end

