function generate_default_legend(n::Int)
    return ["Signal $i" for i in 1:n]
end

# Constants
const EVENT_COLORS = ["red", "blue", "green", "orange", "purple", "cyan", "magenta", "yellow", "black", "gray"]

# Helper function for selecting colors based on indices
function get_color_for_index(index::Integer)
    return EVENT_COLORS[(index % length(EVENT_COLORS)) + 1]
end

# function create_trace(data; mode="lines", name="", color="auto", idx=1, display_legend=false, yaxis="")
#     if color == "auto"
#         color = get_color_for_index(idx)
#     end
#     trace = PlotlyJS.scatter(x=data[:x], y=data[:y], mode=mode, name=name, line=Dict(:color=>color), showlegend=display_legend)
#     if !isempty(yaxis)
#         trace[:yaxis] = yaxis
#     end
#     return trace
# end

function get_timescale_string(time_scale::Symbol)
    if time_scale == :abs
        return "Absolute Time"
    elseif time_scale == :rel
        return "Relative Time"
    elseif time_scale == :channel_rel
        return "Relative Time with respect to Another Channel"
    elseif time_scale == :samples
        return "Sample Number"
    else
        throw(ArgumentError("Invalid time_scale!"))
    end
end

function get_time_scale(obj::Union{Capture{T}, STFT} where T, time_scale; rel_obj:: Union{Nothing, Capture, STFT}=nothing)

    if typeof(obj) <: Capture
        len = length(obj)
        sample_rate = obj.sample_rate
        time_start = obj.time_start
    elseif typeof(obj) <: STFT
        len = size(obj, 2)
        sample_rate = obj.sample_rate
        time_start = obj.time_start
    else
        throw(ArgumentError("Invalid Obj"))
    end

    if !isnothing(rel_obj)
        if typeof(rel_obj) == Capture
            rel_time_start = rel_obj.time_start
        elseif typeof(rel_obj) == STFT
            rel_time_start = rel_obj.time_start
        end
    end

    if time_scale == :abs
        return collect(0:len-1)/sample_rate .+ time_start
    elseif time_scale == :rel
        return collect(0:len-1)/sample_rate
    elseif time_scale == :channel_rel && !isnothing(rel_obj)
        return collect(0:len-1)/sample_rate .+ rel_time_start
    elseif time_scale == :samples
        return collect(0:len-1)
    else
        throw(ArgumentError("Invalid time_scale!"))
    end
end

function get_yaxis_title(symbol, type)
    title_dict = Dict(
        :generic_abs => Dict(
            :abs => "Magnitude",
            :phase => "Phase (Radians)",
            :real => "Real Part",
            :imag => "Imaginary Part",
            :ind => "Imaginary Part Normalized with Frequency",
            :none => "Amplitude"
        ),
        :generic_rel => Dict(
            :abs => "Relative Magnitude",
            :phase => "Relative Phase (Radians)",
            :real => "Relative Real Part",
            :imag => "Relative Imaginary Part",
            :ind => "Relative Imaginary Part Normalized with Frequency",
            :none => "Relative Amplitude"
        ),
        :impedance_abs => Dict(
            :abs => "Impedance Magnitude (Ohms)",
            :phase => "Impedance Phase (Radians)",
            :real => "Real Part of Impedance",
            :imag => "Imaginary Part of Impedance",
            :ind => "Inductance (H)",
            :none => "Amplitude"
        )
    )

    if haskey(title_dict, symbol) && haskey(title_dict[symbol], type)
        return title_dict[symbol][type]
    else
        throw(ArgumentError("Invalid type for the given symbol!"))
    end
end

function make_traces(tds::Vector{Capture{T}} where T ; 
                          alt_x_axis::Union{Vector{<:Vector{<:Number}},Vector{<:Number}, Nothing}=nothing,
                          legend_entries::Vector{<:AbstractString}=generate_default_legend(length(tds)), 
                          plot_as_dots::Bool = false,
                          showlegend::Vector{Bool}=convert(Vector{Bool}, trues(length(tds))),
                          events::Dict{String, Float64}=Dict{String,Float64}(),
                          showlegend_events::Vector{Bool} = convert(Vector{Bool}, trues(length(events))),
                          time_scale::Symbol = :rel,
                          channel_rel_time_ref::Union{Nothing, Integer} = nothing, #  Only relevant when time_scale is :channel_rel
                          colour_idx_offset::Integer = 0)
    # Check if the length of tds and legend_entries are the same
    if length(tds) != length(legend_entries)
        throw(ArgumentError("The number of Capture objects must match the number of legend entries!"))
    end
    

    # Generate traces for each Capture object
    mode = plot_as_dots ? "lines+markers" : "lines"
    traces = Vector{GenericTrace{Dict{Symbol, Any}}}()
    all_y_values = Float64[]  # Collect all y-values to determine the range for vertical lines
    
    for (i, td) in enumerate(tds)
        if isnothing(alt_x_axis)
            time = get_time_scale(td, time_scale, rel_obj = channel_rel_time_ref)
        elseif alt_x_axis isa Vector{<:Vector{<:Number}}
            # Check if the length of alt_x_axis matches the number of Capture objects
            if length(alt_x_axis) != length(tds)
                throw(ArgumentError("The number of alt_x_axis vectors must match the number of Capture objects!"))
            end
            time = alt_x_axis[i]
        elseif alt_x_axis isa Vector{<:Number}
            time = alt_x_axis
        end
        trace = PlotlyJS.scatter(;x=time, y=td.val, mode=mode, name=legend_entries[i], legendgroup=legend_entries[i], showlegend=showlegend[i], line=Dict(:color=>get_color_for_index(i+colour_idx_offset)))
        push!(traces, trace)
        append!(all_y_values, td.val)
    end

    y_min, y_max = extrema(all_y_values)
    
    # Add events as vertical lines
    for (idx, (event_name, event_time)) in enumerate(events)
        event_trace = PlotlyJS.scatter(;
            x=[event_time;event_time], 
            y=[y_min;y_max],
            x1=event_time, 
            mode="line",
            name=event_name,
            legendgroup=event_name,
            showlegend=showlegend_events[idx],
            line=Dict(:color=>get_color_for_index(length(tds) + idx))
        )
        push!(traces, event_trace)
    end

    return traces
end