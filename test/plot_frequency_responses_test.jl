using qd_analysis
using qd_plotter # Assuming qd_plotter includes the plotting functions you've defined

# Generate sine waves and their captures
sineWave = Multisine([0.01], total_length=1000)
sineWave2 = Multisine([0.02], total_length=1000)
sineWave3 = Multisine([0.01, 0.02, 0.05], total_length=1000)

cap1 = Capture(sineWave)
cap2 = Capture(sineWave2)
cap3 = Capture(sineWave3)

cap1_fft = single_sided_fft(cap1)
cap2_fft = single_sided_fft(cap2)
cap3_fft = single_sided_fft(cap3)
cap3_stim_response = extract_frequency_response(cap3)


# Plot a single frequency response with customizations
plot_frequency_responses(
    cap1_fft,
    legend_entries=["Single Sine Wave FFT"],
    plot_title="Single Frequency Response",
    x_limits=(0, 0.05),
    y_limits=(0, 10),
    y_axis_label="Amplitude",
    response_type=:mag,
    plot_as_dots=true,
    y_axis_log=false,
    colour_ids=[1],
    legend_present=BitVector([true])
)

# Plot multiple frequency responses with customizations
plot_frequency_responses(
    [cap1_fft, cap2_fft, cap3_fft],
    legend_entries=["Sine Wave 1 FFT", "Sine Wave 2 FFT", "Sine Wave 3 FFT"],
    plot_title="Multiple Frequency Responses",
    x_limits=(0, 0.06),
    y_limits=(0, 20),
    y_axis_label="Amplitude",
    response_type=:mag,
    plot_as_dots=false,
    y_axis_log=true,
    colour_ids=[1, 2, 3], # Assign unique color IDs for differentiation
    legend_present=BitVector([true, true, true])
)

plot_frequency_responses(
    cap3_stim_response,
    legend_entries=["3 sine wave response"],
    plot_title="Single Frequency Response",
    y_axis_label="Amplitude",
    response_type=:mag,
    plot_as_dots=true,
    colour_ids=[1]
)