using qd_analysis
using qd_plotter # Assuming qd_plotter includes the plotting functions you've defined

# Generate sine waves and their captures
sineWave = Multisine([0.5], total_length=16)

cap = Capture(sineWave)

split_cap = split(cap, 4, 4)
split_cap[1] = split_cap[1]*2

cap_stft = single_sided_fft(split_cap)

event = Dict("Event 1" => 4.0, "Event 2" => 7.0)
plot_frequency_series([cap_stft], events=event, optional_capture = [cap])

# Plot the relative frequency series
plot_rel_frq_series(cap_stft, alt_x_axis=[1.0,2.0,2.5,3.0], type=:abs, mode=:rel, time_scale=:rel)
qd_plotter.plot_rel_frq_series(cap_stft, alt_x_axis=[1.0,2.0,2.5,3.0], type=:abs, mode=:rel, time_scale=:rel)