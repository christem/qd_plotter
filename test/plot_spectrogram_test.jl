using qd_analysis
using qd_plotter # Assuming qd_plotter includes the plotting functions you've defined

# Generate sine waves and their captures
sineWave = Multisine([0.01, 0.02, 0.05], total_length=10000)

cap = Capture(sineWave, sample_rate=10000)

split_cap = split(cap, 500, 500)

cap_stft = single_sided_fft(split_cap)

plot_spectrogram(cap_stft, time_scale=:rel, freq_scale=:log, z_scale=:log)

cap_stft2 = extract_frequency_response(split_cap)

a = collect(0:0.01:1)
plot_rel_frq_series(cap_stft2, auxiliary_data=a, auxiliary_time=a, type=:abs, mode=:rel, time_scale=:rel, auxiliary_name="Auxiliary Data", shaded_region=(0.2, 0.8))