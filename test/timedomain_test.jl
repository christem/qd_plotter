using qd_analysis
using qd_plotter

sineWave = Multisine([0.01], total_length=100)
sineWave2 = Multisine([0.02], total_length=120)

a = collect(1:100)*0.1
b = collect(1:120)*2
cap1 = Capture(sineWave, time_start = 20)
cap2 = Capture(sineWave2, time_start = 0)


# Plot a single time series with no options
plot_time_domain([cap1])

# Plot multiple time series with no options
plot_time_domain([cap1,cap2])

# Plot multiple time series with absolute time
plot_time_domain([cap1,cap2], time_scale=:abs)

# Plot time series with events and title
event = Dict("Event 1" => 50.0, "Event 2" => 70.0)
plot_time_domain([cap1,cap2], legend_entries=["A test1";"Another"], alt_x_axis = [a,b],events=event, plot_title="My Plot", y_limits=(-0.5,0.5), y_axis_title=:impedance_abs, type=:abs)